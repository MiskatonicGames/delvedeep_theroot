﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Storage;
using Microsoft.Xna.Framework.Input;

namespace DelveDeep_TheRoot
{
    public class Rooms
    {
        /*Rooms are Vertices of a graph*/
        //Attributes
        //NOTE: May add more stuff
        string roomName;
        bool visited;
        Texture2D roomTexture;

        /// <summary>
        /// Initializes a new instance of the <see cref="DelveDeep_TheRoot.Rooms"/> class
        /// </summary>
        /// <param name="name">Name of the room</param>
        /// <param name="texture">Texture for room</param>
        public Rooms(string name, Texture2D texture)
        {
            roomName = name;
            roomTexture = texture;
            visited = false;
        }

        /// <summary>
        /// Gets or sets the name of the room
        /// </summary>
        /// <value>The name of the room</value>
        public string RoomName
        {
            get { return roomName; }
            set { roomName = value; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether this <see cref="DelveDeep_TheRoot.Rooms"/> is visited
        /// </summary>
        /// <value><c>true</c> if visited; otherwise, <c>false</c></value>
        public bool Visited
        {
            get { return visited; }
            set { visited = value; }
        }

        /// <summary>
        /// Gets or sets the room texture.
        /// </summary>
        /// <value>The room texture</value>
        public Texture2D RoomTexture
        {
            get { return roomTexture; }
            set { roomTexture = value; }
        }
    }
}