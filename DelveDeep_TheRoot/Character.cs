﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Storage;
using Microsoft.Xna.Framework.Input;

namespace DelveDeep_TheRoot
{
    //Character class inherits from GameObject
    public class Character : GameObject
    {
        KeyboardState kbState;
        KeyboardState prevState;
        MouseState mState;

        enum PlayerState {WalkLeft, WalkRight, IdleLeft, IdleRight};
        PlayerState curState;
        PlayerState lastState;

        Animation flashlightWalk;

        public Character(int x, int y, int width, int height)
            :base(x, y, width, height)
        {
            
        }

        /// <summary>
        /// Load the content
        /// </summary>
        /// <param name="content">Content</param>
        public void Load(ContentManager content)
        {
            texture = content.Load<Texture2D>("Investigator.png");
            flashlightWalk = new Animation(13, 22, 8, 0, texture);
        }

        /// <summary>
        /// Update method
        /// </summary>
        /// <param name="gameTime">Game time</param>
        public void Update(GameTime gameTime)
        {
            Movement(gameTime);
        }

        /// <summary>
        /// Movement method for character
        /// </summary>
        /// <param name="gameTime">Game time.</param>
        public void Movement(GameTime gameTime)
        {
            kbState = Keyboard.GetState();
            mState = Mouse.GetState();

            //Check which key is pressed, and change state accordingly. Run appropriate update methods if moving
            if (kbState.IsKeyDown(Keys.D))
            {
                coords.X += 1;
                DetermineState();
                flashlightWalk.Update(gameTime);
            }

            else if(kbState.IsKeyDown(Keys.A))
            {
                coords.X -= 1;
                DetermineState();
                flashlightWalk.Update(gameTime);
            }

            else
            {
                if(lastState == PlayerState.WalkRight)
                {
                    curState = PlayerState.IdleRight;
                    lastState = curState;
                }

                if(lastState == PlayerState.WalkLeft)
                {
                    curState = PlayerState.IdleLeft;
                    lastState = curState;
                }
            }
        }

        /// <summary>
        /// Determines the player state for walking
        /// </summary>
        public void DetermineState()
        {
            //If the mouse is to the right of the character, face right when walking
            if (mState.Position.X > coords.Center.X)
            {
                curState = PlayerState.WalkRight;
                lastState = curState;
            }

            //Else, face left
            else
            {
                curState = PlayerState.WalkLeft;
                lastState = curState;
            }
        }

        /// <summary>
        /// Draw method
        /// </summary>
        /// <param name="spriteBatch">Sprite batch</param>
        public void Draw(SpriteBatch spriteBatch)
        {
            //Switch for draw based on player state
            switch(curState)
            {
                case(PlayerState.WalkRight):
                    {   
                        flashlightWalk.Animate(spriteBatch, coords, false);
                        break;
                    }

                case(PlayerState.WalkLeft):
                    {
                        flashlightWalk.Animate(spriteBatch, coords, true);
                        break;
                    }

                case(PlayerState.IdleRight):
                    {
                        flashlightWalk.Draw(spriteBatch, coords, false);
                        break;
                    }

                case(PlayerState.IdleLeft):
                    {
                        flashlightWalk.Draw(spriteBatch, coords, true);
                        break;
                    }

                default:
                    flashlightWalk.Draw(spriteBatch, coords, false);
                    break;
            }
        }
    }
}