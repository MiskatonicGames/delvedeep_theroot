﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Storage;
using Microsoft.Xna.Framework.Input;

namespace DelveDeep_TheRoot
{
	public class GameObject
	{
		protected Texture2D texture;
		protected Rectangle coords;

        /// <summary>
        /// Initializes a new instance of the <see cref="DelveDeep_TheRoot.GameObject"/> class
        /// </summary>
        /// <param name="x">The starting x coordinate</param>
        /// <param name="y">The starting y coordinate</param>
        /// <param name="width">Width of the object</param>
        /// <param name="height">Height of the object</param>
		public GameObject(int x, int y, int width, int height)
		{
			coords = new Rectangle(x, y, width, height);
		}

        /// <summary>
        /// Gets or sets the texture.
        /// </summary>
        /// <value>The texture</value>
		public Texture2D Texture
		{
			get { return texture; }
			set { texture = value;}
		}

        /// <summary>
        /// Gets or sets the coords
        /// </summary>
        /// <value>The coords</value>
		public Rectangle Coords
		{
			get { return coords; }
			set { coords = value;}
		}

        /// <summary>
        /// Draw method
        /// </summary>
        /// <param name="spriteBatch">Sprite batch</param>
		public virtual void Draw(SpriteBatch spriteBatch)
		{
			spriteBatch.Draw(texture, coords, Color.White);
		}

        /// <summary>
        /// Collision method
        /// </summary>
        public void Collide()
        {
            //TODO: Collision...
        }
	}
}