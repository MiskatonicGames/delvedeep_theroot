﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Storage;
using Microsoft.Xna.Framework.Input;

namespace DelveDeep_TheRoot
{
    public class LayerStructure
    {
        protected Dictionary<string, List<Rooms>> adjList = new Dictionary<string, List<Rooms>>();
        protected List<Rooms> rooms = new List<Rooms>();

        protected LayerStructure()
        {
        }

        /// <summary>
        /// Adds the room to the list and dictionary
        /// </summary>
        /// <param name="name">Name of the room</param>
        /// <param name="texture">Texture for the room</param>
        protected void AddRoom(string name, Texture2D texture)
        {
            rooms.Add(new Rooms(name, texture));
            adjList.Add(name, new List<Rooms>());
        }

        /// <summary>
        /// Adds the adjacent room to the list
        /// </summary>
        /// <param name="room1">Room to connect</param>
        /// <param name="room2">Room being connected to room1</param>
        protected void AddAdjacentRoom(string room1, Rooms room2)
        {
            adjList[room1].Add(room2);
        }
    }
}

