﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Storage;
using Microsoft.Xna.Framework.Input;

namespace DelveDeep_TheRoot
{
    public class GameState
    {       
        GameStates currGameState = GameStates.MainMenu;

        KeyboardState kbState;
        KeyboardState prevState;
        MouseState mState;
       
        Texture2D cursor;
        Texture2D playNorm;
        Texture2D playOver;
        Rectangle playRect;

        Character player;
        Layer1 l1 = new Layer1();

        /// <summary>
        /// Initializes a new instance of the <see cref="DelveDeep_TheRoot.GameState"/> class
        /// </summary>
        /// <param name="g">GraphicsDevice</param>
        public GameState(GraphicsDevice g)
        {
            playRect = new Rectangle(g.Viewport.Width, g.Viewport.Height / 4, 200, 64);
            player = new Character(0, g.Viewport.Height, 130, 220);
        }

        /// <summary>
        /// Update method
        /// </summary>
        /// <param name="gameTime">Game time</param>
        public void Update(GameTime gameTime)
        {
            //Call other update methods
            ChangeGameState();
            l1.Update();
            player.Update(gameTime);
        }

        /// <summary>
        /// Load the content
        /// </summary>
        /// <param name="Content">Content</param>
        public void Load(ContentManager Content)
        {            
            playNorm = Content.Load<Texture2D>("playNorm.png");
            playOver = Content.Load<Texture2D>("playOver.png");
            cursor = Content.Load<Texture2D>("cursor.png");

            //Call other load methods
            l1.Load(Content);

            player.Load(Content);
        }

        /// <summary>
        /// Draw method
        /// </summary>
        /// <param name="spriteBatch">Sprite batch</param>
        /// <param name="g">GraphicsDevice</param>
        public void Draw(SpriteBatch spriteBatch, GraphicsDevice g)
        {
            //Switch based on current game state
            switch(currGameState)
            {
                case(GameStates.MainMenu):
                    {
                        if(playRect.Contains(mState.Position))
                            spriteBatch.Draw(playOver, playRect, Color.White);

                        else
                            spriteBatch.Draw(playNorm, playRect, Color.White);
                        
                        spriteBatch.Draw(cursor, new Vector2(mState.Position.X, mState.Position.Y), Color.White);
                        break;
                    }
                   
                case(GameStates.InGame):
                    {
                        //Call proper draws
                        l1.Draw(spriteBatch, g);
                        player.Draw(spriteBatch);
                        break;
                    }
            }
        }

        //TODO: Add more switches
        /// <summary>
        /// Changes the state of the game
        /// </summary>
        public void ChangeGameState()
        {
            mState = Mouse.GetState();

            //Switch using current gamestate
            switch(currGameState)
            {
                case(GameStates.MainMenu):
                    {
                        if (playRect.Contains(mState.Position) && mState.LeftButton == ButtonState.Pressed)
                            currGameState = GameStates.InGame;
                        
                        break;
                    }

                case(GameStates.InGame):
                    {

                        break;
                    }
            }
        }
    }
}