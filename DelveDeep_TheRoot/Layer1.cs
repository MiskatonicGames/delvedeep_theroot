﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Storage;
using Microsoft.Xna.Framework.Input;

namespace DelveDeep_TheRoot
{
    public class Layer1 : LayerStructure
    {
        Rooms curRoom;
        Texture2D[] l1Rooms;

        public Layer1()
        {
            //NOTE: Will inevitably change
            l1Rooms = new Texture2D[5];
        }

        /// <summary>
        /// Load the content
        /// </summary>
        /// <param name="Content">Content</param>
        public void Load(ContentManager Content)
        {
            l1Rooms[0] = Content.Load<Texture2D>("testroom.png");
            AddRoom("testroom", l1Rooms[0]);
            curRoom = rooms[0];
        }


        public void Update()
        {
        }

        /// <summary>
        /// Draw the rooms of the layer
        /// </summary>
        /// <param name="spriteBatch">Sprite batch</param>
        /// <param name="g">GraphicsDevice</param>
        public void Draw(SpriteBatch spriteBatch, GraphicsDevice g)
        {
            spriteBatch.Draw(curRoom.RoomTexture, new Rectangle(g.Viewport.X, g.Viewport.Height / 4, g.Viewport.Width, g.Viewport.Height / 2), Color.White);
        }
    }
}

