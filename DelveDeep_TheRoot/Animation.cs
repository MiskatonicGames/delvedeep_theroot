﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Storage;
using Microsoft.Xna.Framework.Input;

namespace DelveDeep_TheRoot
{
    public class Animation
    {
        float frameTime = .2f;
        int frameRow;
        float time;
        int totalFrames;
        int frameColumn;
        int width;
        int height;
        Texture2D spriteSheet;

        /// <summary>
        /// Initializes a new instance of the <see cref="DelveDeep_TheRoot.Animation"/> class
        /// </summary>
        /// <param name="width">Width of individual sprite</param>
        /// <param name="height">Height of individual sprite</param>
        /// <param name="total">Total amount of frames in animation</param>
        /// <param name="frameRow">Row of frame on spritesheet</param>
        /// <param name="spriteSheet">Sprite sheet</param>
        public Animation(int width, int height, int total, int frameRow, Texture2D spriteSheet)
        {
            this.spriteSheet = spriteSheet;
            this.width = width;
            this.height = height;
            this.frameRow = frameRow;
            totalFrames = total;
        }

        /// <summary>
        /// Update method
        /// </summary>
        /// <param name="gameTime">Game time</param>
        public void Update(GameTime gameTime)
        {
            time += (float)gameTime.ElapsedGameTime.TotalSeconds;

            while(time > frameTime)
            {
                frameColumn++;
                time = 0f;
            }

            if (frameColumn > totalFrames)
                frameColumn = 1;
        }

        /// <summary>
        /// Draw method for static sprite
        /// </summary>
        /// <param name="spriteBatch">Sprite batch</param>
        /// <param name="curLoc">Coordinates of Object</param>
        /// <param name="reversed">If set to <c>true</c> reverses sprite</param>
        public void Draw(SpriteBatch spriteBatch, Rectangle curLoc, bool reversed)
        {
            if (reversed)
                spriteBatch.Draw(spriteSheet, curLoc, new Rectangle(0, height * frameRow, width, height), Color.White, 0, Vector2.Zero, SpriteEffects.FlipHorizontally, 0);
            else
                spriteBatch.Draw(spriteSheet, curLoc, new Rectangle(0, height * frameRow, width, height), Color.White);
        }

        /// <summary>
        /// Animation method
        /// </summary>
        /// <param name="spriteBatch">Sprite batch</param>
        /// <param name="curLoc">Coordinates of Object</param>
        /// <param name="reversed">If set to <c>true</c> reverses the animtion</param>
        public void Animate(SpriteBatch spriteBatch, Rectangle curLoc, bool reversed)
        {
            if(reversed)
                spriteBatch.Draw(spriteSheet, curLoc, new Rectangle(width * frameColumn, height * frameRow, width, height), Color.White, 0, Vector2.Zero, SpriteEffects.FlipHorizontally, 0);
            else
                spriteBatch.Draw(spriteSheet, curLoc, new Rectangle(width * frameColumn, height * frameRow, width, height), Color.White);
        }
    }
}