﻿using System;

namespace DelveDeep_TheRoot
{
    public enum GameStates
    { //GameState Enum...
        MainMenu,
        Intro,
        InGame,
        Pause,
        Options,
        Ending
    }
}